import flet as ft
from flet import(
    Checkbox,
    Column,
    FloatingActionButton,
    IconButton,
    OutlinedButton,
    Page,
    Row,
    Tab,
    Text,
    TextField,
    UserControl,
    colors,
    icons
) # importação dos componentes que vão se usar na aplicação

# Classe de tarefas
class Task(UserControl):
     def __init__(self, task_name, task_status_change,task_delete):
          self.completed = False
          super().__init__(*args, **kwargs)
#Classe da aplicação toda

#Função principal da aplicação
def main(page: Page):
     page.title = "Tarefas"
     page.horizontal_alignment = "center"
     page.scroll = "adaptive"
     page.update()

